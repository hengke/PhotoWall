package com.hengke.photowall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/***
 * @date 创建时间 2018/3/25 22:11
 * @author 作者: yulong
 * @description
 */
public class PhotoReceiveBroadcast extends BroadcastReceiver {
    public static final String RECEIVE_PHOTO = "com.hengke.photowall.Receive_photo";
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() == null || intent.getAction().equals(RECEIVE_PHOTO)) {

        }

    }
}
