package com.hengke.photowall;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.hengke.photowall.utils.ALog;
import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.CustomRecyclerView;
import com.hengke.photowall.utils.ImageLoadUtil;
import com.hengke.photowall.utils.SharedPreferencesUtil;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/3/28 21:04
 * @author 作者: yulong
 * @description
 */
public class PhotoShowDialog extends BaseDialog implements View.OnClickListener {
    public static float scale = 0.8f;
    @BindView(R.id.dialog_photo_ViewPager) ViewPager dialogPhotoViewPager;
    PhotoPagerAdapter pagerAdapter;
    @BindView(R.id.dialog_next_photo_ImageView) ImageView nextPhotoImageView;
    @BindView(R.id.dialog_prev_photo_ImageView) ImageView prevPhotoImageView;
    @BindView(R.id.dialog_back_ImageView) ImageView backImageView;
    @BindView(R.id.dialog_photo_list_RecyclerView) CustomRecyclerView dataRecyclerView;
    @BindView(R.id.dialog_photo_view_container_FrameLayout) FrameLayout containerFrameLayout;
    private Animation inAnimation, outAnimation;
    private UploadPhotoAdapter uploadPhotoAdapter;
    private final int MODE_SHOW_PHOTO = 0;
    private final int MODE_PHOTO_LIST = 1;
    private int currentMode;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1){
                if (isShowing()){
                    dismiss();
                }
            }
        }
    };

    public PhotoShowDialog(@NonNull Context context) {
        super(context);
    }


    protected void configDialog() {
        WindowManager.LayoutParams wl = getWindow().getAttributes();
        wl.gravity = Gravity.CENTER;// 设置重力
        getWindow().setWindowAnimations(R.style.centerDialogWindowAnim);
        wl.height = (int) (APPUtility.getScreenHeight() * scale);
        wl.width = (int) (APPUtility.getScreenWidth() * scale);
        wl.y = (int) (APPUtility.getScreenHeight() * ((1 - scale) / 4));
        getWindow().setAttributes(wl);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_show_image_layout);
        ButterKnife.bind(this);
        inAnimation = AnimationUtils.loadAnimation(getContext(),R.anim.anim_scale_in);
        outAnimation = AnimationUtils.loadAnimation(getContext(),R.anim.anim_scale_out);

        scale = (float) SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_ScreenPresent, 80) / 100;
        ALog.debug("屏幕比例为:" + scale);

        nextPhotoImageView.setOnClickListener(this);
        prevPhotoImageView.setOnClickListener(this);
        backImageView.setOnClickListener(this);

        pagerAdapter = new PhotoPagerAdapter();
        dialogPhotoViewPager.setAdapter(pagerAdapter);
        handler.sendEmptyMessageDelayed(1, 5000);
        configDialog();

    }


    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        handler.removeMessages(1);
        handler.sendEmptyMessageDelayed(1, 5000);
        return super.dispatchTouchEvent(ev);
    }


    public void showList(){
        currentMode = MODE_PHOTO_LIST;
        containerFrameLayout.setVisibility(View.GONE);
        uploadPhotoAdapter = new UploadPhotoAdapter(getContext());
        dataRecyclerView.setAdapter(uploadPhotoAdapter);
        uploadPhotoAdapter.setOnPictureItemClickListener(onPhotoCLickListener);
    }

    public void showClickPhoto(List<File> fileList, int position) {
        pagerAdapter.setPhotoFileList(fileList);
        dialogPhotoViewPager.setCurrentItem(position);
        currentMode = MODE_SHOW_PHOTO;
    }

    //查看模式下的点击图片列表的放大显示
    private UploadPhotoAdapter.OnPhotoCLickListener onPhotoCLickListener = (position, list) -> {
        currentMode = MODE_PHOTO_LIST;
        pagerAdapter.setPhotoFileList(list);
        dialogPhotoViewPager.setCurrentItem(position, false);
        containerFrameLayout.setVisibility(View.VISIBLE);
        containerFrameLayout.startAnimation(inAnimation);
    };



    @Override
    public void onClick(View v) {
        if (v == nextPhotoImageView) {
            dialogPhotoViewPager.setCurrentItem(dialogPhotoViewPager.getCurrentItem() + 1, true);
        } else if (v == prevPhotoImageView) {
            dialogPhotoViewPager.setCurrentItem(dialogPhotoViewPager.getCurrentItem() - 1, true);
        } else if (v == backImageView) {
            if (currentMode == MODE_PHOTO_LIST && containerFrameLayout.getVisibility() == View.VISIBLE){
                containerFrameLayout.startAnimation(outAnimation);
                outAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        containerFrameLayout.setVisibility(View.GONE);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            } else {
                dismiss();
            }
        }
    }


    private class PhotoPagerAdapter extends PagerAdapter {
        private List<File> photoFileList;

        public void setPhotoFileList(List<File> photoFileList) {
            this.photoFileList = photoFileList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (photoFileList == null) {
                return 0;
            }
            return photoFileList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image_view_layout, container, false);
            ImageView imageView = view.findViewById(R.id.item_photo_ImageView);
            ImageLoadUtil.loadImageFromPath(imageView, photoFileList.get(position).getAbsolutePath(),
                    (int)(APPUtility.getScreenWidth() * scale), (int)(APPUtility.getScreenHeight() * scale));
            container.addView(view);
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
