package com.hengke.photowall.service;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.hengke.photowall.R;
import com.hengke.photowall.network.Configuration;
import com.hengke.photowall.network.HostAddress;
import com.hengke.photowall.network.sender.SendConnectClient;
import com.hengke.photowall.network.sender.SendFileClient;
import com.hengke.photowall.utils.ALog;
import com.hengke.photowall.utils.APPUtility;

import java.io.File;
import java.util.ArrayList;

/***
 * @date 创建时间 2018/3/23 22:13
 * @author 作者: yulong
 * @description
 */
public class SharkListenService extends Service {
    private static final int SENSOR_SHAKE = 5;
    private static final int MSG_TYPE_SUCCESS = 1;
    private static final int MSG_TYPE_FAILURE = -1;
    private SensorManager sensorManager;
    private Vibrator vibrator;
    private long fileSize;
    private ConnectThread connectThread ;
    private volatile boolean isRunning = false;
    /**
     * 重力感应监听
     */
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        boolean isVibrating = false;

        @Override
        public void onSensorChanged(SensorEvent event) {
            // 传感器信息改变时执行该方法
            float[] values = event.values;
            float x = values[0]; // x轴方向的重力加速度，向右为正
            float y = values[1]; // y轴方向的重力加速度，向前为正
            float z = values[2]; // z轴方向的重力加速度，向上为正

            // 一般在这三个方向的重力加速度达到40就达到了摇晃手机的状态。
            int medumValue = 19;

            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                if (Math.abs(x) > medumValue || Math.abs(y) > medumValue || Math.abs(z) > medumValue) {
                    if (!isRunning) {
                        vibrator.vibrate(300);
                        ALog.debug("照片路径:" + getRecentlyPhotoPath());
                        APPUtility.showToast("正在发送照片...", false);

                        File file = new File(getRecentlyPhotoPath());
                        ArrayList<File> fileList = new ArrayList<>();
                        fileList.add(file);
                        isRunning = true;
                        connectThread = new ConnectThread();
                        connectThread.setFileList(fileList);
                        connectThread.start();
                    }
                    else {
                        vibrator.cancel();

                        vibrator.vibrate(new long[]{200, 200, 200, 200, 200, 200}, -1);
                        APPUtility.showToast("已经有发送任务了,请等待发送任务完成！", false);
                    }

                }

            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_TYPE_SUCCESS) {
                playSound(true);
                if (msg.obj != null){
                    APPUtility.showToast(msg.obj.toString(), false);
                }
            }
            else if (msg.what == MSG_TYPE_FAILURE) {
                if (msg.obj != null){
                    if (msg.arg1 == 11){
                        playSound(false);
                        APPUtility.showToast(msg.obj.toString(), true);
                    } else {
                        APPUtility.showToast(msg.obj.toString(), false);
                    }
                }
            }

        }
    };

    private void playSound(boolean isSuccess) {
        try {
            int id = isSuccess ? R.raw.success : R.raw.failure;
            MediaPlayer mediaPlayer = MediaPlayer.create(this, id);//重新设置要播放的音频
            mediaPlayer.start();//开始播放
        } catch (Exception e) {
            e.printStackTrace();//输出异常信息
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        if (sensorManager != null) {// 注册监听器
            sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
            // 第一个参数是Listener，第二个参数是所得传感器类型，第三个参数值获取传感器信息的频率
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sensorManager != null) {// 取消监听器
            sensorManager.unregisterListener(sensorEventListener);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String getRecentlyPhotoPath() {
        String searchPath = MediaStore.Files.FileColumns.DATA + " LIKE '%" + "/DCIM/Camera/" + "%' ";
        Uri uri = MediaStore.Files.getContentUri("external");
        Cursor cursor = getContentResolver().query(
                uri, new String[]{MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns.SIZE}, searchPath, null, MediaStore.Files.FileColumns.DATE_ADDED + " DESC");
        String filePath = "";
        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));
            fileSize = cursor.getLong(cursor.getColumnIndex(MediaStore.Files.FileColumns.SIZE));
            //Log.i(LOG_TAG, "图片大小："+fileSize);
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return filePath;
    }



    private class ConnectThread extends Thread {
        SendConnectClient udpClient;
        ArrayList<File> fileList;

        public ConnectThread() {
            udpClient = new SendConnectClient();
        }

        public void setFileList(ArrayList<File> fileList) {
            this.fileList = fileList;
        }

        @Override
        public void run() {
            super.run();
            isRunning = true;
            HostAddress hostAddress = udpClient.search();
            if (hostAddress == null) {
//                Toast.makeText(context, "请先搜索主机", Toast.LENGTH_LONG).show();
                ALog.debug("失败，请先搜索主机");
                Message msg = Message.obtain();
                msg.what = MSG_TYPE_FAILURE;
                msg.arg1 = 11;
                msg.obj = "发送失败了,请联系工作人员，确认是否已连接设备";
                handler.sendMessage(msg);
                isRunning = false;
                return;
            }

            final String addressStr = hostAddress.getAddress().toString();
            final int port = hostAddress.getPort();
            ALog.debug("找到主机,IP 地址:" + addressStr + ",TCP端口:" + port);

            SendFileClient tcpClient = new SendFileClient(hostAddress);

            String fileinfo = "";
            for (int position = 0; fileList != null && position < fileList.size(); position++) {
                fileinfo += (fileList.get(position).getName() + Configuration.FILE_LEN_SPT + fileList.get(position).length() + Configuration.FILES_SPT);
            }
            fileinfo += Configuration.DELIMITER;

            String replyMsg = tcpClient.connectReceiver(fileinfo);
            int finished = 0;
            if (replyMsg.length() > 0) {
                finished = tcpClient.sendFile(fileList);
                ALog.debug("发送成功");
                Message msg = Message.obtain();
                msg.what = MSG_TYPE_SUCCESS;
                msg.obj = "发送成功";
                handler.sendMessage(msg);
            }
            else {
                Message msg = Message.obtain();
                msg.what = MSG_TYPE_FAILURE;
                msg.obj = "发送失败了..";
                handler.sendMessage(msg);

                ALog.debug("接收方没有确认");
//                return -2;//接收方没有确认
            }
            tcpClient.close();

            isRunning = false;

        }
    }


}
