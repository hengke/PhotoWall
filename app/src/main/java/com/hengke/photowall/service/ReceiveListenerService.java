package com.hengke.photowall.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.hengke.photowall.MainActivity;
import com.hengke.photowall.network.receiver.ReceiveFileTask;
import com.hengke.photowall.utils.ALog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/***
 * @date 创建时间 2018/3/25 16:29
 * @author 作者: yulong
 * @description 接受图片的后台服务监听
 */
public class ReceiveListenerService extends Service implements ReceiveFileTask.OnReceiveConnectListener{
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onReceiveConnectListener();
        return super.onStartCommand(intent, flags, startId);
    }
    ReceiveFileTask receiveConnectTask;
    private int index = 0;
    private void onReceiveConnectListener(){
        if (receiveConnectTask != null){
            receiveConnectTask.cancel(true);
            receiveConnectTask = null;
        }

        receiveConnectTask = new ReceiveFileTask();
        receiveConnectTask.setOnReceiveConnectListener(this);
        receiveConnectTask.execute();

        ALog.debug(index++ + "、等待连接," + sdf.format(Calendar.getInstance().getTime()));
    }


    @Override
    public void onceAgain() {
        onReceiveConnectListener();
    }

    @Override
    public void onSuccess(String name) {
        ALog.w("接收成功,文件名为:" + name);

        Intent intent = new Intent();
        intent.setAction(MainActivity.RECEIVE_PHOTO);
        intent.putExtra("FileName", name);
        sendBroadcast(intent);
    }
}
