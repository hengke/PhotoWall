package com.hengke.photowall;

import java.io.File;
import java.util.Comparator;

/***
 * @date 创建时间 2018/3/26 22:24
 * @author 作者: yulong
 * @description
 */
public class FileComparator implements Comparator<File> {
    //是否倒序
    boolean isDesc;

    public FileComparator(boolean isDesc) {
        this.isDesc = isDesc;
    }

    @Override
    public int compare(File lhs, File rhs) {
        int result = 0;
        if (lhs.lastModified() > rhs.lastModified()) {
            result = isDesc ? 1 : -1;
        }
        else {
            result = isDesc ? -1 : 1;
        }
        return result;
    }
}
