package com.hengke.photowall.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.hengke.photowall.base.APP;

import java.io.InputStream;

/**
 * Created by yulong on 2018/1/26.
 */

public class APPUtility {


    private static int screenWidth = 0, screenHeight = 0;

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dp2px(float dpValue) {
        float scale = APP.getInstance().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale);
    }

    public static int getScreenWidth() {
        if (screenWidth <= 1) {
            initScreenSize();
        }
        return screenWidth;
    }

    public static int getScreenHeight() {
        if (screenHeight <= 1) {
            initScreenSize();
        }
        return screenHeight;
    }


    private static void initScreenSize() {
        WindowManager manager = (WindowManager) APP.getInstance().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        screenWidth = outMetrics.widthPixels;
        screenHeight = outMetrics.heightPixels;
    }

    //转换图片
    public static Bitmap loadBitmapFromView(View v) {
        int w = v.getWidth();
        int h = v.getHeight();
        Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmp);
        c.drawColor(Color.WHITE);
        /** 如果不设置canvas画布为白色，则生成透明 */
        v.layout(0, 0, w, h);
        v.draw(c);
        //压缩
        Matrix matrix = new Matrix();
        matrix.setScale(0.2f, 0.2f);
        ALog.debug("压缩前图片大小:" + bmp.getByteCount());

        // 得到新的图片
        return Bitmap.createBitmap(bmp, 0, 0, w, h, matrix,
                true);
//        return bmp;
    }


    public static void showToast(String text, boolean isLong) {
        Toast.makeText(APP.getInstance(), text, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    /**
     * 以最省内存的方式读取本地资源的图片
     *
     * @param resId
     * @return
     */
    public static Bitmap loadBitmap(int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        // 获取资源图片
        InputStream is = APP.getInstance().getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }
}
