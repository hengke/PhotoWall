package com.hengke.photowall.utils;

import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Created by yulong on 2018/3/18.
 */

public class CustomLayoutManager extends StaggeredGridLayoutManager {
    private boolean isScrollEnabled = true;

    public CustomLayoutManager(int spanCount, int orientation) {
        super(spanCount, orientation);
    }


    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    @Override
    public boolean canScrollVertically() {
        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        return isScrollEnabled && super.canScrollVertically();
    }
}
