package com.hengke.photowall.utils;

import android.os.Environment;

import com.hengke.photowall.FileComparator;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by yulong on 2018/3/17.
 */

public class PathUtil {


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    private static String userPath;
    public static String getUserPath(){
        if (userPath == null){
            userPath = getRootPath() + sdf.format(Calendar.getInstance().getTime());
            checkOrMakeDir(userPath);
        }
        return userPath;
    }

    private static String rootPath;
    public static String getRootPath(){
        if (rootPath == null){
            rootPath = Environment.getExternalStorageDirectory().getPath() + "/APhotoWall/";
            checkOrMakeDir(rootPath);
        }
        return rootPath;
    }


    private static String logPath;
    public static String getLogPath(){
        if (logPath == null){
            logPath = getRootPath() + "Log/" + sdf.format(Calendar.getInstance().getTime())+"/";
            checkOrMakeDir(logPath);
        }
        return logPath;
    }

    private static File logFile;
    public static File getLogFile(){
        if (logFile == null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
            logFile = new File(getLogPath() + "log_"+sdf.format(Calendar.getInstance().getTime()) +".txt");
        }
        return logFile;
    }

    private static void checkOrMakeDir(String path) {
        File file = new File(path);
        if (!file.exists()){
            file.mkdir();
        }
    }

    private static String demoPhotoPath;
    public static String getDemoPhotoPath(){
        if (demoPhotoPath == null){
            demoPhotoPath = getRootPath() + "Demo";
            checkOrMakeDir(demoPhotoPath);
        }
        return demoPhotoPath;
    }


    public static List<File> loadFileFromSDCard(boolean isDesc) {
        List<File> list = new ArrayList<>();
        File dirFile = new File(getRootPath());
        if (dirFile.exists()) {
            File[] listFile = dirFile.listFiles(new PhotoFileFilter());


            if (listFile != null) {
                List<File> sourceFileList = Arrays.asList(listFile);
                Collections.sort(sourceFileList, new FileComparator(false));

                int max = 30;
                int i = 0;
                for (File f : sourceFileList) {

                    if (i < max) {
                        list.add(f);
                    }
                    else {
                        ALog.d("删除了文件。" + f.getName());
                        f.delete();
                    }
                    i++;
                }
                Collections.sort(list, new FileComparator(isDesc));
            }
        }
        return list;
    }
}
