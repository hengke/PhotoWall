package com.hengke.photowall.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hengke.photowall.base.APP;


public class SharedPreferencesUtil {
    public static final String KEY_MaxCount = "MaxCount";
    public static final String KEY_RowCount = "RowCount";
    public static final String KEY_ScreenPresent= "ScreenPresent";

    private static SharedPreferences sharedPreferences;

    private static SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            synchronized (SharedPreferencesUtil.class) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(APP.getInstance());
            }
        }
        return sharedPreferences;
    }

    private static SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

    public static void setSharedPreferences(String key, String value) {
        if (key == null || value == null) {
            return;
        }
        getEditor().putString(key, value).commit();
    }

    public static void putBoolean(String key, boolean value) {
        if (key == null) {
            return;
        }
        getEditor().putBoolean(key, value).commit();
    }


    public static void putInt(String key, int value){
        getEditor().putInt(key, value).commit();
    }

    public static void putString(String key, String value){
        getEditor().putString(key, value).commit();
    }

    public static String getString(String key, String defValue) {
        if (key == null || defValue == null) {
            return "";
        }
        return getSharedPreferences().getString(key, defValue);
    }


    public static int getInt(String key, int defValue){
        return getSharedPreferences().getInt(key, defValue);
    }


    public static boolean getBoolean(String key, boolean defValue) {
        if (key == null) {
            return false;
        }
        return getSharedPreferences().getBoolean(key, defValue);
    }


}
