package com.hengke.photowall.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hengke.photowall.base.APP;

/**
 * Created by yulong on 2018/3/18.
 */

public class ImageLoadUtil {


    public static void loadImageFromPath(ImageView v, String path, float v1) {
        Glide.with(v.getContext()).load(path).asBitmap()//.format(DecodeFormat.PREFER_RGB_565)
                .thumbnail(0.1f).into(v);
    }
    public static void loadImageFromPath(ImageView v, String path, int width, int height) {
        Glide.with(v.getContext()).load(path).asBitmap()//.format(DecodeFormat.PREFER_RGB_565)
                .override(width, height).thumbnail(0.1f).into(v);
    }

    public static void loadImageFromPath(String path, int width, int height,SimpleTarget<Bitmap> simpleTarget) {
        Glide.with(APP.getInstance()).load(path).asBitmap()//.format(DecodeFormat.PREFER_RGB_565)
                .override(width, height).thumbnail(0.1f).into(simpleTarget);
    }

    public static float getImageSizeScale(String path) {
        //创建bitmap工厂的配置参数
        BitmapFactory.Options options = new BitmapFactory.Options();

        //返回一个null 没有bitmap   不去真正解析位图 但是能返回图片的一些信息(宽和高)
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        //[3]获取图片的宽和高
        float imgWidth = options.outWidth;
        float imgHeight = options.outHeight;
        float rate = imgHeight / imgWidth;
        if (rate > 12.0f / 9) {
            rate = 12.0f / 9;
        }
        else if (rate < 9.0f / 12) {
            rate = 9.0f / 12;
        }
        return rate;
    }


    public static void getBitmapFromUrl(String path, SimpleTarget<Bitmap> simpleTarget){
        Glide.with(APP.getInstance()).load(path)
                .asBitmap().into(simpleTarget);
    }

    public static void getBitmapFromUrl(String path, int defaultImg, SimpleTarget<Bitmap> simpleTarget){
        Glide.with(APP.getInstance()).load(path)
                .asBitmap().placeholder(defaultImg).into(simpleTarget);
    }
}
