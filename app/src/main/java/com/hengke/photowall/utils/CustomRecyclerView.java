package com.hengke.photowall.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;

import com.hengke.photowall.R;


public class CustomRecyclerView extends RecyclerView {

    private int layoutStyle;
    private int spanCount = 2;
    private int divideLine;
    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomRecyclerView);

        layoutStyle = typedArray.getInt(R.styleable.CustomRecyclerView_layout_style,0);
        divideLine = typedArray.getInt(R.styleable.CustomRecyclerView_divide_line_style,0);
        spanCount = typedArray.getInteger(R.styleable.CustomRecyclerView_spanCount, 2);
        typedArray.recycle();

        setLayoutStyle(layoutStyle);
        setDivideLineStyle(divideLine);
    }

    private void setDivideLineStyle(int divideLine) {
        if (divideLine != 0){
            int orientation ;
            if (divideLine == 1){
                orientation = LinearLayoutManager.VERTICAL;
            }else {
                orientation = LinearLayoutManager.HORIZONTAL;
            }
            DividerLine dividerItemDecoration = new DividerLine(orientation);
            addItemDecoration(dividerItemDecoration);
        }

    }

    public void setLayoutStyle(int layoutStyle){
        LayoutManager layoutManager;
        switch (layoutStyle){
            case 0:
                layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                break;
            case 2:
                layoutManager = new GridLayoutManager(getContext(),spanCount, GridLayoutManager.VERTICAL, false);
                break;
            case 3:
                layoutManager = new GridLayoutManager(getContext(),spanCount, GridLayoutManager.HORIZONTAL, false);
                break;
            case 9:
                layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
                break;
            case 1:
            default: //默认的就是列表模式
                layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                break;
        }
        setLayoutManager(layoutManager);
    }


    public static class DividerLine extends ItemDecoration {
        /**
         * 水平方向
         */
        public static final int HORIZONTAL = LinearLayoutManager.HORIZONTAL;

        /**
         * 垂直方向
         */
        public static final int VERTICAL = LinearLayoutManager.VERTICAL;

        // 画笔
        private Paint paint;

        // 布局方向
        private int orientation;
        // 分割线颜色
        private int color;
        // 分割线尺寸
        private int size = 2;

        public DividerLine() {
            this(VERTICAL);
        }

        public DividerLine(int orientation) {
            this.orientation = orientation;

            paint = new Paint();
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDrawOver(c, parent, state);

            if (orientation == HORIZONTAL) {
                drawHorizontal(c, parent);
            } else {
                drawVertical(c, parent);
            }
        }

        /**
         * 设置分割线颜色
         *
         * @param color 颜色
         */
        public void setColor(int color) {
            this.color = color;
            paint.setColor(color);
        }

        /**
         * 设置分割线尺寸
         *
         * @param size 尺寸
         */
        public void setSize(int size) {
            this.size = size;
        }

        // 绘制垂直分割线
        protected void drawVertical(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + size;

                c.drawRect(left, top, right, bottom, paint);
            }
        }

        // 绘制水平分割线
        protected void drawHorizontal(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + size;

                c.drawRect(left, top, right, bottom, paint);
            }
        }
    }
}
