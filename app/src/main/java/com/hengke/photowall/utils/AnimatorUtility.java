package com.hengke.photowall.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by yulong on 2018/2/2.
 */

public class AnimatorUtility {

    public static void alphaAnimator(View v,Animator.AnimatorListener animatorListener, long time, float... alphaValues){
        ObjectAnimator alphaOutAnimator = ObjectAnimator.ofFloat(v, "alpha", alphaValues);
        if (animatorListener != null){
            alphaOutAnimator.addListener(animatorListener);
        }
        alphaOutAnimator.setDuration(time);
        alphaOutAnimator.start();
    }

    public static void translateYAnimator(View v, Animator.AnimatorListener animatorListener, long time, float... values){
        ObjectAnimator translateInAnimator = ObjectAnimator.ofFloat(v,
                "translationY", values);
        if (animatorListener != null){
            translateInAnimator.addListener(animatorListener);
        }
        translateInAnimator.setDuration(time);
        translateInAnimator.start();
    }


//    public static void alphaAnimator(View v, Animator.AnimatorListener animatorListener, long time){
//        ObjectAnimator alphaOutAnimator = ObjectAnimator.ofFloat(v, "alpha", 1, 0);
//        if (animatorListener != null){
//            alphaOutAnimator.addListener(animatorListener);
//        }
//        alphaOutAnimator.setDuration(time);
//        ObjectAnimator alphaInAnimator = ObjectAnimator.ofFloat(v, "alpha", 0,1);
//        alphaInAnimator.setDuration(time);
//
//        AnimatorSet animatorSet = new AnimatorSet();//组合动画
//        animatorSet.setDuration(1000);
//        animatorSet.setInterpolator(new DecelerateInterpolator());
//        animatorSet.play(alphaOutAnimator).after(time).with(alphaInAnimator);
//        animatorSet.start();
//    }

    public static void scaleAnimator(View v, Animator.AnimatorListener animatorListener){
        scaleAnimator(v, 1.2f, animatorListener);
    }

    public static void scaleAnimator(View v, float scale, Animator.AnimatorListener animatorListener){
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(v, "scaleX", 1, scale,1);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(v, "scaleY", 1, scale,1);

        AnimatorSet animatorSet = new AnimatorSet();//组合动画
        animatorSet.setDuration(300);
        if (animatorListener != null){
            animatorSet.addListener(animatorListener);
        }
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.play(scaleXAnimator).with(scaleYAnimator);
        animatorSet.start();

    }

    private static final int alphaAnimTime = 200;


    public static void changeGifImageWithAlphaAnim(final GifImageView gifImageView, final GifDrawable gifDrawable) {
        ObjectAnimator alphaOutAnimator = ObjectAnimator.ofFloat(gifImageView, "alpha", 1, 0.5f);
        alphaOutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                gifImageView.setImageDrawable(gifDrawable);
                ObjectAnimator.ofFloat(gifImageView, "alpha", 0.5f, 1)
                        .setDuration(alphaAnimTime).start();
            }
        });
        alphaOutAnimator.setDuration(alphaAnimTime);
        alphaOutAnimator.start();
    }

    public static void changeImageWithAlphaAnim(final ImageView view, final int resId) {
        changeImageWithAlphaAnim(view, resId, alphaAnimTime);
    }

    public static void changeImageWithAlphaAnim(final ImageView view, final int resId, final long time) {
        ObjectAnimator alphaOutAnimator = ObjectAnimator.ofFloat(view, "alpha", 1, 0.5f);
        alphaOutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Bitmap bitmap = APPUtility.loadBitmap(resId);
                if(view.getLayoutParams() instanceof RelativeLayout.LayoutParams){
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    params.height = bitmap.getHeight();
                    view.setLayoutParams(params);
                }
                view.setImageBitmap(bitmap);

                ObjectAnimator.ofFloat(view, "alpha", 0.5f, 1)
                        .setDuration(time).start();
            }
        });
        alphaOutAnimator.setDuration(time);
        alphaOutAnimator.start();
    }


    public static void startAlphaRepeat(View view){
        startAlphaRepeat(view, 2000);
    }

    public static void startAlphaRepeat(View view, long time){
        startAlphaRepeat(view, time, 1, 0,1);
    }

    public static void startAlphaRepeat(View view, long time, float... values){
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", values);
        objectAnimator.setDuration(time);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.setRepeatCount(-1);
        objectAnimator.start();
    }
}
