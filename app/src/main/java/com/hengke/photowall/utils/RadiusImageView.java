package com.hengke.photowall.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/***
 * @date 创建时间 2018/3/29 21:40
 * @author 作者: yulong
 * @description
 */
@SuppressLint("AppCompatCustomView")
public class RadiusImageView extends ImageView{

    Paint paint = new Paint();
    public RadiusImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint.setAntiAlias(true);
    }

//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//
//        RectF r2=new RectF();                           //RectF对象
//        r2.left=0;                                 //左边
//        r2.top=0;                                 //上边
//        r2.right=getWidth();                                   //右边
//        r2.bottom=getHeight();                              //下边
//        canvas.drawRoundRect(r2, 10, 10, paint);        //绘制圆角矩形
//
//    }




    /**
     * 绘制圆形图片
     * @author caizhiming
     */
    @Override
    protected void onDraw(Canvas canvas) {

        Drawable drawable = getDrawable();
        if (null != drawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Bitmap b = getCircleBitmap(bitmap, getWidth());
            final Rect rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            final RectF rectDest = new RectF(0,0,getWidth(),getHeight());
            canvas.drawRoundRect(rectDest, 15, 15, paint);
            paint.reset();

            canvas.drawBitmap(b, rectSrc,rectDest, paint);
//            canvas.drawBitmap(b, rectSrc, rectDest, paint);

        } else {
            super.onDraw(canvas);
        }
    }


    /**
     * 获取圆形图片方法
     * @param bitmap
     * @param pixels
     * @return Bitmap
     * @author caizhiming
     */
    private Bitmap getCircleBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, 15, 15, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rectF, paint);
        return output;


    }
}
