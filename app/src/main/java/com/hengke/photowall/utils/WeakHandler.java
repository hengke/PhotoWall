package com.hengke.photowall.utils;

import android.os.Handler;

import java.lang.ref.WeakReference;


public class WeakHandler<T> extends Handler {
    private WeakReference<T> weakReference;
    private T instance;
    public T getInstance(){
        if (instance == null){
            instance = weakReference.get();
        }
        return instance;
    }

    public WeakHandler(T t) {
        weakReference = new WeakReference<T>(t);
    }

}
