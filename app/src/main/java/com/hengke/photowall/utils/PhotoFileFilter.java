package com.hengke.photowall.utils;

import java.io.File;
import java.io.FileFilter;

/***
 * @date 创建时间 2018/3/29 22:22
 * @author 作者: yulong
 * @description
 */
public class PhotoFileFilter implements FileFilter {
    @Override
    public boolean accept(File pathname) {
        String fileName = pathname.getName();
        return fileName.endsWith(".jpg") || fileName.endsWith(".png") ||
                fileName.endsWith(".jpeg");
    }
}
