package com.hengke.photowall.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

/***
 *@date 创建时间 2018/3/25 20:49
 *@author 作者: yulong
 *@description  
 */
public class HostAddress {
    private InetAddress address;
    private int port;

    public HostAddress(InetAddress address, int port) {
        this.address = address;
        this.port = port;
    }

    public HostAddress(String ip, int port) {
        try {
            this.address = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.port = port;
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "HostAddress{" +
                "address=" + address +
                ", port=" + port +
                '}';
    }
}
