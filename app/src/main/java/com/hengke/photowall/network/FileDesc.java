package com.hengke.photowall.network;

import java.io.Serializable;

/***
 *@date 创建时间 2018/3/25 16:23
 *@author 作者: yulong
 *@description  
 */
public class FileDesc implements Serializable {
    //用户生成文件的描述信息发送给
    private String name;
    private long length;

    public FileDesc(String name, long length) {
        super();
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "FileDesc [name=" + name + ", length=" + length + "]";
    }
}
