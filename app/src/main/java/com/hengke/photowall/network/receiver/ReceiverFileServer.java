package com.hengke.photowall.network.receiver;

import com.hengke.photowall.network.Configuration;
import com.hengke.photowall.network.FileDesc;
import com.hengke.photowall.network.Utils;
import com.hengke.photowall.utils.ALog;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

/**
 * Created by xanarry on 2016/5/22.
 */
public class ReceiverFileServer {
//    private ProgressListener progressListener;
    private int port;
    private String TAG;
    private ServerSocket serverSocket;
    private Socket channel;
    private BufferedInputStream bufferedInputStream;
    private BufferedOutputStream bufferedOutputStream;

    public ReceiverFileServer(int port) {
//        this.progressListener = progressListener;
        this.port = port;
        TAG = ReceiverFileServer.class.getName();
    }


    public ArrayList<FileDesc> waitSenderConnect() {
        ArrayList<FileDesc> fileDescs = null;
        String fileInfo = "";
        byte[] inputBuf = new byte[Configuration.STRING_BUF_LEN];

        try {
            serverSocket = new ServerSocket(this.port);//创建tcp服务器, 接收文件
            serverSocket.setSoTimeout(Configuration.WAITING_TIME * 1000);//设置等待连接时长
            ALog.w(TAG, "tcp server is waiting");
            channel = serverSocket.accept();//建立链接
            channel.setKeepAlive(Boolean.TRUE);
            ALog.w("serverSocket.accept()");

            //获取socket的输入输出流
            bufferedInputStream = new BufferedInputStream(channel.getInputStream());
            bufferedOutputStream = new BufferedOutputStream(channel.getOutputStream());

            bufferedInputStream.read(inputBuf);//读取要接收文件的描述信息<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            fileInfo = Utils.getMessage(inputBuf);
            ALog.debug("fileinfo: " + fileInfo);
            //分离字符串形式的文件描述信息, 保持到arraylist
            fileDescs = new ArrayList<>();
            for (String file : fileInfo.split(Configuration.FILES_SPT)) {
                String[] fd = file.split(Configuration.FILE_LEN_SPT);
                fileDescs.add(new FileDesc(fd[0], Long.parseLong(fd[1])));
            }

            //将客户端发送的信息原封回复, 表示可以开始传输文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            bufferedOutputStream.write((Utils.getMessage(inputBuf) + Configuration.DELIMITER).getBytes("utf-8"));
            ALog.debug("将客户端发送的信息原封回复, 表示可以开始传输文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            bufferedOutputStream.flush();
        } catch (SocketTimeoutException e) {
            try {
                serverSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileDescs;
    }

    public int recieveFile(ArrayList<FileDesc> files, String savePath) {
        byte[] recvBuf = new byte[Configuration.STRING_BUF_LEN];
        int filePosition = 0;
        String msg = "";

        for (; filePosition < files.size(); filePosition++) {
            try {
                long hasRecieve = 0;
                int actualLen;
                FileDesc fileDesc = files.get(filePosition);

                FileOutputStream fileOutputStream = null;
                File newFile = new File(savePath, fileDesc.getName());
                newFile.createNewFile();
                newFile.setWritable(true);
                fileOutputStream = new FileOutputStream(newFile);
                ALog.d("接收即将要发送的文件");
                bufferedInputStream.read(recvBuf);//接收即将要发送的文件<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                msg = Utils.getMessage(recvBuf);
                msg += Configuration.DELIMITER;
                ALog.d("发送准备接收的确认,msg:" + msg);

                //发送准备接收的确认>>>>>>>>>>>>>>>>>>>>>>>>>>>
                bufferedOutputStream.write(msg.getBytes("utf-8"));
                bufferedOutputStream.flush();

//                startTime = System.nanoTime();
                if (fileDesc.getLength() == 0) { //接收空文件处理
                    continue;
                }

                //从网络中读取文件字节流
                byte[] fileBuf = new byte[Configuration.FILE_IO_BUF_LEN];
                while ((actualLen = bufferedInputStream.read(fileBuf, 0, Configuration.FILE_IO_BUF_LEN)) > 0) {
                    //将网络中的字节流写入本地文件
                    fileOutputStream.write(fileBuf, 0, actualLen);
                    hasRecieve += actualLen;
                    // recieve all part of file
                    if (hasRecieve == fileDesc.getLength()) {
                        bufferedOutputStream.flush();
                        ALog.debug("接收完成");
                        break;
                    }
                }

                if (hasRecieve == fileDesc.getLength()) {
                    String sizeAck = hasRecieve + Configuration.DELIMITER;
                    bufferedOutputStream.write(sizeAck.getBytes("utf-8"));
                    bufferedOutputStream.flush();
                }

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                ALog.e("FileNotFoundException :" + e1.getMessage());
                continue;
            } catch (IOException e) {
                e.printStackTrace();
                ALog.e("出现了异常,IOException:"+e.getMessage());
            }
        }
        return filePosition;
    }

    public void close() {
        try {
            bufferedInputStream.close();
            bufferedOutputStream.close();
            serverSocket.close();
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
