package com.hengke.photowall.network;

/***
 *@date 创建时间 2018/3/25 20:56
 *@author 作者: yulong
 *@description  
 */
public interface SearchStateListener {
    void updateState(int tryTimes, int times);
}