package com.hengke.photowall.network.sender;

import android.os.AsyncTask;

import com.hengke.photowall.network.Configuration;
import com.hengke.photowall.network.HostAddress;
import com.hengke.photowall.utils.ALog;

import java.io.File;
import java.util.ArrayList;

/***
 * @date 创建时间 2018/3/25 17:03
 * @author 作者: yulong
 * @description
 */
public class SendFileTask extends AsyncTask<HostAddress, Integer, Integer> {
    boolean isSending;
    @Override
    protected void onPreExecute() {
        isSending = true;
    }
    private ArrayList<File> fileList;

    public void setFile(ArrayList<File> file) {
        this.fileList = file;
    }

    @Override
    protected Integer doInBackground(HostAddress... params) {
        HostAddress receiverAddr = params[0];
        if (receiverAddr == null) {
            return -1;
        }

        SendFileClient tcpClient = new SendFileClient(receiverAddr);

        String fileinfo = "";
        for (int position = 0; fileList != null && position < fileList.size(); position++) {
            fileinfo += (fileList.get(position).getName() + Configuration.FILE_LEN_SPT + fileList.get(position).length() + Configuration.FILES_SPT);
        }
        fileinfo += Configuration.DELIMITER;

        String replyMsg = tcpClient.connectReceiver(fileinfo);
        int finished = 0;
        if (replyMsg.length() > 0) {
            finished = tcpClient.sendFile(fileList);
            ALog.debug("发送成功");
        } else {
            ALog.debug("接收方没有确认");
            return -2;//接收方没有确认
        }
        tcpClient.close();
        return finished;
    }

    @Override
    protected void onPostExecute(Integer finishedCount) {
        isSending = false;

        ALog.debug("发送结果:" + finishedCount);
        if (finishedCount < 0){

        }
    }
}
