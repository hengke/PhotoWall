package com.hengke.photowall.network.sender;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hengke.photowall.network.HostAddress;
import com.hengke.photowall.utils.ALog;

import java.io.File;
import java.util.ArrayList;

/***
 * @date 创建时间 2018/3/24 22:17
 * @author 作者: yulong
 * @description
 */
public class SearchReceiverTask extends AsyncTask<String, Integer, HostAddress> {
//    private ProgressDialog searchDialog;
    private Context context;
    SendConnectClient udpClient;
    private File file;

    public SearchReceiverTask(Context context, File file) {
        this.context = context;
        this.file = file;
        udpClient = new SendConnectClient();
    }


    @Override
    protected HostAddress doInBackground(String... params) {
        return udpClient.search();
    }


    @Override
    protected void onPostExecute(final HostAddress hostAddress) {
        if (hostAddress == null){
            Toast.makeText(context, "请先搜索主机", Toast.LENGTH_LONG).show();
            return;
        }

        final String addressStr = hostAddress.getAddress().toString();
        final int port = hostAddress.getPort();
        ALog.debug("找到主机\nIP 地址:" + addressStr + ",TCP端口:" + port);


        ArrayList<File> fileList = new ArrayList<>();
        fileList.add(file);

        SendFileTask sendFileTask = new SendFileTask();
        sendFileTask.setFile(fileList);
        sendFileTask.execute(hostAddress);

    }
}
