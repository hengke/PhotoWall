package com.hengke.photowall.network.receiver;

import com.hengke.photowall.network.Configuration;
import com.hengke.photowall.network.Utils;
import com.hengke.photowall.utils.ALog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Created by xanarry on 2016/5/22.
 */
public class ReceiverConnectServer {
//    private ProgressListener progressListener;
    private int port;
    private String TAG;
    DatagramSocket serverSockent;

    public ReceiverConnectServer(int port) {
        this.port = port;
        this.TAG = ReceiverConnectServer.class.getName();
//        this.progressListener = progressListener;
    }

    public DatagramPacket waitClient() {
        //server wait and receive message broadcasted by client
        DatagramPacket packet = null;
        byte[] recvBuf = new byte[Configuration.STRING_BUF_LEN];
        try {
            serverSockent = new DatagramSocket(port);//设置服务器端口, 监听广播信息
            serverSockent.setSoTimeout(30 * 60* 1000); //等待半小时
            DatagramPacket message = new DatagramPacket(recvBuf, recvBuf.length);
            serverSockent.receive(message);//接收client的广播信息
            String msgStr = Utils.getMessage(message.getData());
            ALog.debug("DatagramPacket message:" + msgStr);
            //将服务器的主机名发送给client
            message.setData((Configuration.currentTcpPort + Configuration.DELIMITER).getBytes("utf-8"));
            serverSockent.send(message);//回复信息tcp要使用的Tcp端口给client

            message.setData(msgStr.getBytes("utf-8"));
            packet = message;
        } catch (SocketTimeoutException e) {
//            progressListener.updateProgress(-3, 100, 100, 999);
            ALog.e("udp server 等待超时");
        } catch (UnsupportedEncodingException e) {
            ALog.e("编码解释错误" + e.getMessage());
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return packet;//将client发送的数据包返回给调用者, 里面包含client的地址, 端口, 主机名
    }

    public void close() {
        serverSockent.close();
    }
}
