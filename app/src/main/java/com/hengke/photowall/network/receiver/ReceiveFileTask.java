package com.hengke.photowall.network.receiver;

import android.os.AsyncTask;

import com.hengke.photowall.network.Configuration;
import com.hengke.photowall.network.FileDesc;
import com.hengke.photowall.utils.ALog;
import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.PathUtil;

import java.net.DatagramPacket;
import java.util.ArrayList;

/***
 * @date 创建时间 2018/3/25 16:33
 * @author 作者: yulong
 * @description
 */
public class ReceiveFileTask extends AsyncTask<String, Integer, String> {
    private String senderIP;
    private ReceiverConnectServer udpServer;
    private ReceiverFileServer tcpServer;
    private ArrayList<FileDesc> fileDescList;
    private OnReceiveConnectListener onReceiveConnectListener;

    public ReceiveFileTask() {
        //等待主机被发现
        udpServer = new ReceiverConnectServer(Configuration.UDP_PORT);
    }

    @Override
    protected String doInBackground(String... strings) {
        long lastTime = System.currentTimeMillis();
        DatagramPacket senderPacket = udpServer.waitClient();
        udpServer.close();
        if (senderPacket == null) {
            return null;//超时得到null
        }

        long successTime = System.currentTimeMillis();
        ALog.d("连接成功:" + successTime + "，耗时:" + (successTime - lastTime));
        senderIP = senderPacket.getAddress().getHostName();
        ALog.debug("senderIP:" + senderIP);
        //启动Tcp server
        tcpServer = new ReceiverFileServer(Configuration.currentTcpPort);

        //等待与发送方建立Tcp连接
        fileDescList = tcpServer.waitSenderConnect();
        if (fileDescList == null || fileDescList.size() == 0) {
            return null;//等待连接失败或
        }

        tcpServer.recieveFile(fileDescList, PathUtil.getUserPath());

        return fileDescList.get(0).getName();
    }

    @Override
    protected void onPostExecute(String name) {
        super.onPostExecute(name);
        if (name != null && tcpServer != null) {
            tcpServer.close();
            APPUtility.showToast("接收成功:" + fileDescList.get(0).getName(), false);
            ALog.debug("接收成功:" + fileDescList.get(0).getName());
            if (onReceiveConnectListener != null) {
                onReceiveConnectListener.onSuccess(name);
            }


        }

        if (onReceiveConnectListener != null) {
            onReceiveConnectListener.onceAgain();
        }

    }

    public void setOnReceiveConnectListener(OnReceiveConnectListener onReceiveConnectListener) {
        this.onReceiveConnectListener = onReceiveConnectListener;
    }

    public interface OnReceiveConnectListener {
        void onceAgain();

        void onSuccess(String name);
    }


}
