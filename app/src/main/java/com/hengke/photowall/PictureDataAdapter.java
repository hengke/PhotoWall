package com.hengke.photowall;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.ImageLoadUtil;
import com.hengke.photowall.utils.PathUtil;
import com.hengke.photowall.utils.PhotoFileFilter;
import com.hengke.photowall.utils.SharedPreferencesUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by yulong on 2018/3/15.
 */

public class PictureDataAdapter extends RecyclerView.Adapter<PictureDataAdapter.PictureViewHolder> {
    private Context context;
    private int rowCount = 3;
    private List<File> AllFileList; //= new ArrayList<>();
    private String newPath;
    private Random random = new Random();
    private int randomSize = 2;
    private float[] scaleArray = {
            0.8f, 0.75f, 0.8f,
            0.95f, 0.9f, 1.0f, 1.05f, 1.1f,
            1.15f,
    };
    private float[] rotationArray = {-12, -10, -6, -4, -3 -5,-2, -1,0, 1,2,3,4,5, 7, 9, 10};

    public PictureDataAdapter(Context context) {
        this.context = context;
        rowCount = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_RowCount, 3);
        initPhotoFile();
    }

    public void updateNewPhoto(String path) {
        newPath = path;
    }

    public void initPhotoFile() {
//        AllFileList = PathUtil.loadFileFromSDCard(true);
        initFiles();
        notifyDataSetChanged();
    }

    int maxSize = 30;
    private void initFiles() {
        maxSize = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_MaxCount, 1);

        File demoDir = new File(PathUtil.getDemoPhotoPath());
        File[] demoFileArray = demoDir.listFiles(new PhotoFileFilter());

        List<File> demoFileList = Arrays.asList(demoFileArray);
        Collections.sort(demoFileList, new FileComparator(false));

        File photoDir = new File(PathUtil.getUserPath());
        File[] photoFileArray = photoDir.listFiles(new PhotoFileFilter());
        List<File> photoList = new ArrayList<>();
        if (photoFileArray != null){
            int len = photoFileArray.length > maxSize ? maxSize: photoFileArray.length;
            for (int i = 0; i < len; i++){
                photoList.add(photoFileArray[i]);
            }
        }
        Collections.sort(photoList, new FileComparator(false));

        int photoSize = photoList.size();

        AllFileList = new ArrayList<>();

        int index = 0;
        int demoSize = demoFileList.size();
        int length = demoSize < maxSize ? maxSize : demoSize;
        for (int i = 0; i < length; i++){
            if (demoSize != 0){
                AllFileList.add(demoFileList.get(i % demoSize));
            }
            if (photoSize > 0){
                for (int j = 0, len = random.nextInt(randomSize) + 1; j < len; j++) {
                    if (index > photoSize - 1) {
                        index = 0;
                    }
                    AllFileList.add(photoList.get(index));
                    index++;
                }

            }

        }
    }

    @Override
    public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PictureViewHolder viewHolder = new PictureViewHolder(context, parent);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PictureViewHolder holder, final int pos) {
        final int realPosition = pos % AllFileList.size();
        final String path = AllFileList.get(realPosition).getAbsolutePath();

        ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();//得到item的LayoutParams布局参数
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = (int) (APPUtility.getScreenWidth() * ImageLoadUtil.getImageSizeScale(path)) / rowCount;
        holder.itemView.setLayoutParams(params);//把params设置给item布局

        float dot = scaleArray[random.nextInt(scaleArray.length)];
        float rotation = rotationArray[random.nextInt(rotationArray.length)];

        holder.itemView.setScaleY(dot);
        holder.itemView.setScaleX(dot);
//        holder.itemView.setRotation(rotation);
        holder.containerLayout.setRotation(rotation);

        FrameLayout.LayoutParams photoParams = (FrameLayout.LayoutParams) holder.photoImageViewView.getLayoutParams();
        photoParams.width = APPUtility.getScreenWidth() / rowCount - APPUtility.dp2px(5) * 2;
        photoParams.height = (int) (photoParams.width * ImageLoadUtil.getImageSizeScale(path));
        holder.photoImageViewView.setLayoutParams(photoParams);

        ImageLoadUtil.loadImageFromPath(path, photoParams.width, photoParams.height, new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                holder.photoImageViewView.setImageBitmap(resource);
            }
        });


        if (newPath != null && path.contains(newPath)) {
//                mListener.ItemClickListener(holder.photoImageViewView, pos, newPath);//把事件交给我们实现的接口那里处理
            holder.photoImageViewView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.photoImageViewView.performClick();
                }
            }, 1);
            newPath = null;
        }

        holder.photoImageViewView.setOnClickListener(v -> {
            PhotoShowDialog photoDialog = new PhotoShowDialog(v.getContext());
            photoDialog.show();
            photoDialog.showClickPhoto(AllFileList, realPosition);
        });
    }



    public Bitmap getRoundCornerBitmap(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        float radius = APPUtility.dp2px(10);

        Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        final RectF rectF = new RectF(0, 0, w, h);
        float rate = 1.0f;
//        if (viewWidth > 0){
//            rate = (float) w / viewWidth; // 这个的乘法是因为如果图片过大，圆角就看起来很小，图片小圆角过大
//        }

        canvas.drawRoundRect(rectF, radius * rate, radius * rate, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, null, rectF, paint);

        /**
         * here to define your corners, this is for left bottom and right bottom corners
         */
        final Rect clipRect = new Rect(0, 0, w, (int) (h - radius));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        canvas.drawRect(clipRect, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, null, rectF, paint);
        return output;
    }

    @Override
    public int getItemCount() {
        if (AllFileList == null || AllFileList.size() == 0) {
            return 0;
        }
        return Integer.MAX_VALUE;//AllFileList.size();
    }

    public static class PictureViewHolder extends RecyclerView.ViewHolder {
        ImageView photoImageViewView;
        FrameLayout containerLayout;
//        CardView cardView;

        public PictureViewHolder(Context context, ViewGroup viewGroup) {
            this(LayoutInflater.from(context).inflate(R.layout.main_item_imageview_layout, viewGroup, false));
        }

        private PictureViewHolder(View itemView) {
            super(itemView);
            photoImageViewView = itemView.findViewById(R.id.item_ImageView);
            containerLayout = itemView.findViewById(R.id.imageview_container_layout);
        }
    }


}

