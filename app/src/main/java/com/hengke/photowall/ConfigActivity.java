package com.hengke.photowall;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hengke.photowall.base.BaseActivity;
import com.hengke.photowall.utils.SharedPreferencesUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/3/29 22:52
 * @author 作者: yulong
 * @description
 */
public class ConfigActivity extends BaseActivity {
    private final int TYPE_MAX_COUNT = 1;
    private final int TYPE_ROW_COUNT = 2;
    private final int TYPE_PRESENT_SCREEN = 3;

    @BindView(R.id.config_load_photo_max_count_TextView) TextView maxCountTextView;
    @BindView(R.id.config_load_photo_max_count_SeekBar) SeekBar maxCountSeekBar;
    @BindView(R.id.config_row_count_TextView) TextView rowCountTextView;
    @BindView(R.id.config_row_count_SeekBar) SeekBar rowCountSeekBar;
    @BindView(R.id.config_present_screen_TextView) TextView presentScreenTextView;
    @BindView(R.id.config_present_screen_SeekBar) SeekBar presentScreenSeekBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_layout);
        ButterKnife.bind(this);
        initUI();
        maxCountSeekBar.setOnSeekBarChangeListener(getSeekBarListener(TYPE_MAX_COUNT));
        rowCountSeekBar.setOnSeekBarChangeListener(getSeekBarListener(TYPE_ROW_COUNT));
        presentScreenSeekBar.setOnSeekBarChangeListener(getSeekBarListener(TYPE_PRESENT_SCREEN));


    }

    private void initUI(){
        int maxCount = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_MaxCount, 1);
        maxCountTextView.setText(String.format("加载最新相片数量: %d", maxCount));
        maxCountSeekBar.setProgress(maxCount - 1);

        int rowCount = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_RowCount, 2);
        rowCountTextView.setText(String.format("照片墙显示几列照片: %d", rowCount));
        rowCountSeekBar.setProgress(rowCount - 2);

        int screenPresent = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_ScreenPresent, 50);
        presentScreenTextView.setText(String.format("查看图片的对话框占比屏幕百分比: %d", screenPresent));
        presentScreenSeekBar.setProgress(screenPresent - 50);

    }


    private SeekBar.OnSeekBarChangeListener getSeekBarListener(int type) {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int value;
                switch (type) {
                    case TYPE_MAX_COUNT:
                        value = seekBar.getProgress() + 1;
                        maxCountTextView.setText(String.format("加载最新相片数量: %d", value));
                        SharedPreferencesUtil.putInt(SharedPreferencesUtil.KEY_MaxCount, value);

                        break;
                    case TYPE_ROW_COUNT:
                        value = seekBar.getProgress() + 2;
                        SharedPreferencesUtil.putInt(SharedPreferencesUtil.KEY_RowCount, value);
                        rowCountTextView.setText(String.format("照片墙显示几列照片: %d", value ));
                        break;
                    case TYPE_PRESENT_SCREEN:
                        value = seekBar.getProgress() + 50;
                        SharedPreferencesUtil.putInt(SharedPreferencesUtil.KEY_ScreenPresent, value);
                        presentScreenTextView.setText(String.format("查看图片的对话框占比屏幕百分比: %d", value));
                        break;
                }

            }
        };
    }


}
