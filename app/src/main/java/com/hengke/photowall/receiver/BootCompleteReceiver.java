package com.hengke.photowall.receiver;

/**
 * Created by Administrator on 2016/12/21.
 */

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import com.hengke.photowall.SplashActivity;
import com.hengke.photowall.utils.ALog;

public class BootCompleteReceiver extends BroadcastReceiver {
    private final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isPhotoWall = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SplashActivity.SP_KEY, false);

        if (!isPhotoWall && ACTION_BOOT.equals(intent.getAction())){
            //开机启动服务
            Intent it = new Intent();
            it.setComponent(new ComponentName("com.hengke.photowall",
                    "com.hengke.photowall.service.SharkListenService"));
            context.startService(it);
            ALog.debug("开机启动服务:" + getClass().getName());
        }
    }
}
