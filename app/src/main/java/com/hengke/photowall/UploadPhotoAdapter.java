package com.hengke.photowall;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.ImageLoadUtil;
import com.hengke.photowall.utils.PathUtil;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yulong on 2018/3/17.
 */
public class UploadPhotoAdapter extends RecyclerView.Adapter<UploadPhotoAdapter.UploadImageViewHolder> {
    private Context context;
    private OnPhotoCLickListener onPictureItemClickListener;
    private List<File> photoFileList;// = new ArrayList<>();

    public UploadPhotoAdapter(Context context) {
        this.context = context;
        initPhotoFile();
    }

    public void setOnPictureItemClickListener(OnPhotoCLickListener onPictureItemClickListener) {
        this.onPictureItemClickListener = onPictureItemClickListener;
    }

    public void initPhotoFile() {
        photoFileList = PathUtil.loadFileFromSDCard(false);
        notifyDataSetChanged();
    }


    @Override
    public UploadImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UploadImageViewHolder(context, parent);
    }

    @Override
    public void onBindViewHolder(final UploadImageViewHolder holder, final int position) {
        ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();
        params.height = params.width = (int) (APPUtility.getScreenWidth() * PhotoShowDialog.scale / 3);
        holder.itemView.setLayoutParams(params);

        int realPosition = position % photoFileList.size();
        final String filePath = photoFileList.get(realPosition).getAbsolutePath();

        ImageLoadUtil.loadImageFromPath(holder.itemPhotoImageView, filePath,
                params.width, params.height);

        holder.itemPhotoImageView.setOnClickListener(v -> {
            if (onPictureItemClickListener != null) {
                onPictureItemClickListener.onPhotoClick(realPosition, photoFileList);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (photoFileList == null || photoFileList.size() == 0) {
            return 0;
        }
        return Integer.MAX_VALUE; //photoFileList.size();
    }

    public interface OnPhotoCLickListener {
        void onPhotoClick(int position, List<File> list);
    }

    public static class UploadImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sub_item_photo_ImageView) ImageView itemPhotoImageView;

        public UploadImageViewHolder(Context context, ViewGroup viewGroup) {
            super(LayoutInflater.from(context).inflate(R.layout.sub_image_view_layout, viewGroup, false));
            ButterKnife.bind(this, this.itemView);
        }
    }
}
