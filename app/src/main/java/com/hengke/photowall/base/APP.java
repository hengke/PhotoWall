package com.hengke.photowall.base;

import android.app.Application;

import com.hengke.photowall.crash.CrashHandler;

/**
 * Created by yulong on 2018/3/15.
 */

public class APP extends Application {

    public static APP instance;

    public static APP getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(this));
    }
}
