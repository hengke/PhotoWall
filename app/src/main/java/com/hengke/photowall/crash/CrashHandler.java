package com.hengke.photowall.crash;

import android.content.Context;
import android.os.Environment;

import com.hengke.photowall.utils.ALog;
import com.hengke.photowall.utils.PathUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/***
 * @date 创建时间 2018/3/29 00:24
 * @author 作者: yulong
 * @description
 */
public class CrashHandler implements Thread.UncaughtExceptionHandler {
    Thread.UncaughtExceptionHandler systemUncaughtExceptionHandler;
//    private Context context;
    public CrashHandler(Context context){
//        this.context = context;
        if (systemUncaughtExceptionHandler == null) {
            systemUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        }
    }

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy_M_d_HH:mm:ss");

    private String getLogInfo(){
        StringBuilder log = new StringBuilder();
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
            }
            log.append("\n\n\n");

        } catch (Exception e) {
        }
        return log.toString();
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        if (e != null) {
            saveCrashInfo2File(e);

        }

        if (systemUncaughtExceptionHandler != null) {
            systemUncaughtExceptionHandler.uncaughtException(t, e);
        }

    }
    //用来存储设备信息和异常信息
    private Map<String, String> infos = new HashMap<String, String>();

    /**
     * 保存错误信息到文件中
     * @return 返回文件名称, 便于将文件传送到服务器
     */
    private String saveCrashInfo2File(Throwable ex) {
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : infos.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            sb.append(key + "=" + value + "\n");
        }

        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        Throwable cause = ex.getCause();
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();
        String result = writer.toString();
        sb.append(result);
        try {
            String str = sdf.format(Calendar.getInstance().getTime());
            String fileName = "crash_" + str  + ".log";
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                String path = PathUtil.getLogPath();
                File dir = new File(PathUtil.getLogPath());
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                FileOutputStream fos = new FileOutputStream(path + fileName);
                //写正常的Logcat
                fos.write(getLogInfo().getBytes());

                //写Crash的Log
                fos.write(sb.toString().getBytes());
                System.out.println(sb.toString());
                fos.close();

                ALog.writeLogToFile(-1, "Crash", sb.toString() +"\n\n\n\n\n\n\n");

            }
            return fileName;
        } catch (Exception e) {
            ALog.e("", "an error occured while writing file...", e);
        }
        return null;
    }


}
