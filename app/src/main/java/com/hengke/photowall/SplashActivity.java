package com.hengke.photowall;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.hengke.photowall.base.BaseActivity;
import com.hengke.photowall.service.SharkListenService;
import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.permission.PermissionListener;
import com.hengke.photowall.utils.permission.PermissionsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yulong on 2018/3/17.
 */

public class SplashActivity extends BaseActivity implements View.OnClickListener{

    @BindView(R.id.photo_wall_Button) Button photoWallButton;
    @BindView(R.id.phone_take_photo_Button) Button takePhotoButton;
    @BindView(R.id.config_View) View configView;
    @BindView(R.id.kill_app_View) View killAPPView;
    SharedPreferences sp ;
    public static final String SP_KEY = "isPhotoWall";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_laout);
        ButterKnife.bind(this);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        photoWallButton.setOnClickListener(this);
        takePhotoButton.setOnClickListener(this);
        configView.setOnClickListener(this);
        killAPPView.setOnClickListener(this);

        checkPermission();
    }


    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private void checkPermission() {
        if (!PermissionsUtil.hasPermission(this, permissions)) {
            PermissionsUtil.PermissionTipsInfo tipsInfo = new PermissionsUtil.PermissionTipsInfo(
                    "注意", "照片墙需要您的SD卡读取权限", "取消", "打开权限"
            );

            PermissionsUtil.requestPermission(this, new PermissionListener() {
                @Override
                public void permissionGranted(@NonNull String[] permission) {
                    if (sp.getBoolean(SP_KEY, false)){
                        gotoMainActivity();
                    }
                }

                @Override
                public void permissionDenied(@NonNull String[] permission) {
                    APPUtility.showToast("您拒绝了权限", false);
                }
            }, permissions, false, null);
        }
    }


    @Override
    public void onClick(View v) {
        if (!PermissionsUtil.hasPermission(this, permissions)){
            APPUtility.showToast("您未获得权限，请先申请权限",false);
            return;
        }
        if (v == photoWallButton){
            gotoMainActivity();
            sp.edit().putBoolean(SP_KEY, true).commit();
        }
        else if (v == takePhotoButton){
            Intent intent = new Intent(SplashActivity.this, SharkListenService.class);
            startService(intent);
            finish();
        }
        else if (v == configView){
            if (System.currentTimeMillis() - lastClickTime < 500){
                Intent intent = new Intent(this, ConfigActivity.class);
                startActivity(intent);
            }
            lastClickTime = System.currentTimeMillis();
        }
        else if (v == killAPPView){
            if (System.currentTimeMillis() - lastClickTime < 500){
                finish();
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
            }
            lastClickTime = System.currentTimeMillis();
        }
    }
    private long lastClickTime = 0;

    private void gotoMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
