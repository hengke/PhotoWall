package com.hengke.photowall;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.hengke.photowall.base.BaseActivity;
import com.hengke.photowall.service.ReceiveListenerService;
import com.hengke.photowall.utils.ALog;
import com.hengke.photowall.utils.APPUtility;
import com.hengke.photowall.utils.CustomLayoutManager;
import com.hengke.photowall.utils.SharedPreferencesUtil;
import com.hengke.photowall.utils.WeakHandler;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    public static final String RECEIVE_PHOTO = "com.hengke.photowall.Receive_photo";
    private static final int MSG_AUTO_SCROLL = 1;
    public PhotoReceiveBroadcast photoReceiveBroadcast;
    @BindView(R.id.main_data_RecyclerView) RecyclerView dataRecyclerView;
    @BindView(R.id.main_look_my_upload_Images) ImageView lookUploadImageView;
    CustomLayoutManager customLayoutManager;
    private PictureDataAdapter dataAdapter;
    private WeakHandler<MainActivity> handler = new WeakHandler<MainActivity>(this) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (getInstance() != null) {
                getInstance().handlerMessageAction(msg);
            }
        }
    };
    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (customLayoutManager.findFirstVisibleItemPositions(null)[0] >= recyclerView.getAdapter().getItemCount() - 10) {
                recyclerView.scrollToPosition(0);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        ButterKnife.bind(this);
        int count = SharedPreferencesUtil.getInt(SharedPreferencesUtil.KEY_RowCount, 3);
        customLayoutManager = new CustomLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
        dataRecyclerView.setLayoutManager(customLayoutManager);
        dataRecyclerView.addOnScrollListener(onScrollListener);
        ALog.debug("进入MainActivity");
        dataAdapter = new PictureDataAdapter(this);
        dataRecyclerView.setAdapter(dataAdapter);

        lookUploadImageView.setOnClickListener(this);

        handler.sendEmptyMessageDelayed(MSG_AUTO_SCROLL, 10);

        Intent intent = new Intent(this, ReceiveListenerService.class);
        startService(intent);
        photoReceiveBroadcast = new PhotoReceiveBroadcast();



    }

    @SuppressLint("NewApi")
    private void handlerMessageAction(Message msg) {
        if (msg.what == MSG_AUTO_SCROLL) {
            if (!isFinishing() && !isDestroyed()) {
                dataRecyclerView.scrollBy(0, 3);
                handler.sendEmptyMessageDelayed(MSG_AUTO_SCROLL, 30);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == lookUploadImageView) {
            showUploadView();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(photoReceiveBroadcast);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(photoReceiveBroadcast, new IntentFilter(RECEIVE_PHOTO));
    }


    private void showUploadView() {
        PhotoShowDialog photoShowDialog = new PhotoShowDialog(this);
        photoShowDialog.show();
        photoShowDialog.showList();
    }

    public class PhotoReceiveBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null || intent.getAction().equals(RECEIVE_PHOTO)) {
                String name = intent.getStringExtra("FileName");
                if (name != null) {
                    ALog.debug("接收的文件名字为:" + name);
                    dataAdapter.updateNewPhoto(name);
                }
//                uploadPhotoAdapter.initPhotoFile();
                dataAdapter.initPhotoFile();

//                dataRecyclerView.scrollToPosition(dataAdapter.currentNewPathIndex());
                APPUtility.showToast("收到图片更新", false);
            }

        }
    }


}
